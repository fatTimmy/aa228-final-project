"""
Classes supporting training and playing flappy bird using DQN.
"""

import time
from enum import Enum

import grpc
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from grpc.beta import implementations
from ple import PLE
from ple.games.flappybird import FlappyBird
from scipy.misc import imresize
from tensorflow_serving.apis import predict_pb2, prediction_service_pb2


class Actions(Enum):
    """
    The possible actions to take.
    """

    UP_ACTION = 119
    NO_ACTION = 0

    @staticmethod
    def random_action():
        """
        Chooses a random action to take.
        """
        return np.random.choice(Actions)

    @staticmethod
    def get_action_from_index(index):
        """
        Gets the action based on the index of the action in the total list of
        possible actions.
        """
        return list(Actions)[index]


class Data(object):
    """
    Represents a data point for batch RL consisting of (s, a, r, sp, terminal).
    """

    @staticmethod
    def make_state(previous_observation, current_observation):
        """
        Creates a flattened state from 2 consecutive observations.
        The unflattened state has the shape [image_height, image_width, image_depth].
        """
        stack = np.stack((previous_observation, current_observation))
        stack_transposed = stack.transpose((1, 2, 0))
        stack_flattened = np.reshape(stack_transposed, [-1])
        return 1.0 * stack_flattened

    @staticmethod
    def transform_observation(observation, image_shrink_factor):
        """
        Shrinks an observation to a smaller size and also turns it greyscale.
        """
        observation = np.transpose(observation, (1, 0, 2))
        def rgb2gray(rgb):
            """
            Creates a grayscale version of an RGB image.
            """
            return np.dot(rgb[..., :3], [0.299, 0.587, 0.114])
        observation = rgb2gray(observation)
        observation = imresize(observation, (observation.shape[0] / image_shrink_factor, observation.shape[1] / image_shrink_factor))
        return observation


    def __init__(self, previous_observation, current_observation, action, reward, next_observation, is_terminal):
        self.s = Data.make_state(previous_observation, current_observation)
        self.a = action
        self.r = reward
        self.sp = Data.make_state(current_observation, next_observation)
        self.is_terminal = is_terminal

class DataBuffer(object):
    """
    A replay buffer of data that has the ability to restrict the number of
    samples stored, as well as randomly sample a batch of data.
    """
    def __init__(self, capacity):
        self.capacity = capacity
        self.buffer = []

    def store_data(self, previous_observation, current_observation, action, reward, next_observation, is_terminal):
        """
        Stores a data sample. Will only store the sample if previous_observation, current_observation, and
        next_observation are all available.
        
        Returns whether any data was stored.
        """
        if all(v is not None for v in [previous_observation, current_observation, next_observation]):
            if len(self.buffer) >= self.capacity:
                self.buffer = self.buffer[1:]
            self.buffer.append(Data(previous_observation, current_observation, action, reward, next_observation, is_terminal))
            return True
        else:
            return False

    def get_batch(self, batch_size):
        """
        Gets a batch of examples from the buffer. Each example is sampled with replacement.
        """
        return np.random.choice(self.buffer, size=batch_size, replace=True)

    def get_size(self):
        """
        Gets the current size of the replay buffer
        """
        return len(self.buffer)

class GameLoop(object):
    """
    The main game loop that allows for training and playing of the game.
    """
    def __init__(
            self, image_width, image_height, image_shrink_factor,
            buffer_size, epsilon, rpc_timeout, serving_update_iterations,
            train_steps_per_iteration, min_buffer_size_for_train,
            display_screen, pipe_gap, pause, learning_rate, gamma,
            tmp_model_dir, exported_model_dir, iterations_per_train, train_batch_size):
        self.exported_model_dir = exported_model_dir
        self.image_width = image_width
        self.image_height = image_height
        self.flappy_bird = FlappyBird(width=image_width, height=image_height, pipe_gap=pipe_gap)
        self.environment = PLE(self.flappy_bird, fps=30, display_screen=display_screen)
        self.environment.init()
        self.data_buffer = DataBuffer(buffer_size)
        self.environment.reset_game()
        self.previous_observation = None
        self.current_observation = None
        self.next_observation = None
        self.iteration = 0
        self.epsilon = epsilon
        self.gamma = gamma
        self.serving_update_iterations = serving_update_iterations
        self.train_steps_per_iteration = train_steps_per_iteration
        self.min_buffer_size_for_train = min_buffer_size_for_train
        self.cumulative_rewards_for_current_episode = 0
        self.cumulative_rewards_per_episode = []
        self.iterations_for_current_episode = 0
        self.iterations_per_episode = []
        self.rpc_timeout = rpc_timeout
        self.image_shrink_factor = image_shrink_factor
        self.iterations_per_train = iterations_per_train
        self.train_batch_size = train_batch_size

        # seconds of pause between each
        self.pause = pause

        # tf learn estimator
        self.estimator = tf.estimator.Estimator(
            model_fn=self.model_fn,
            model_dir=tmp_model_dir,
            params={
                'learning_rate': learning_rate
            }
        )

        self.channel = implementations.insecure_channel("localhost", int(9000))
        self.stub = prediction_service_pb2.beta_create_PredictionService_stub(self.channel)

    def reset_environment(self):
        """
        Resets the game environment for a new episode and resets our state about
        the previous episode.
        """
        self.environment.reset_game()
        self.previous_observation = None
        self.current_observation = None
        self.next_observation = None
        self.cumulative_rewards_for_current_episode = 0
        self.iterations_for_current_episode = 0

    def iterate(self):
        """
        Runs one iteration of the game frame in training mode. Knows to reset
        the game if we're at the end of an episode.
        """
        if self.iteration % 100 == 0:
            print("iteration %d " % self.iteration)

        # set up the environment for this iteration
        self.set_up_environment_for_iteration()

        # select an action based on the current and previous observations
        action = self.get_action()

        # receive reward for action
        reward = self.environment.act(action)
        self.update_cumulative_rewards(reward)

        # make next observation
        self.next_observation = Data.transform_observation(self.environment.getScreenRGB(), self.image_shrink_factor)

        # store transition to buffer
        self.data_buffer.store_data(
            self.previous_observation, self.current_observation, action, reward, self.next_observation,
            self.environment.game_over())

        # improve current model once in a while
        if (self.iteration + 1) % self.iterations_per_train == 0:
            print("training current model")
            self.train()

        # update serving model to current model once in a while
        if (self.iteration + 1) % self.serving_update_iterations == 0:
            print("updating serving model to current model")
            self.update_serving_model()

        if self.pause > 0:
            time.sleep(self.pause)

    def play_episode(self):
        """
        Plays an episode of the game.
        """
        self.reset_environment()

        while not self.environment.game_over():
            action = self.get_action()
            reward = self.environment.act(action)
            self.cumulative_rewards_for_current_episode += reward
            self.next_observation = Data.transform_observation(self.environment.getScreenRGB(), self.image_shrink_factor)
            self.previous_observation, self.current_observation = self.current_observation, self.next_observation
            if self.pause > 0:
                time.sleep(self.pause)
        self.cumulative_rewards_per_episode.append(self.cumulative_rewards_for_current_episode)
        print(self.cumulative_rewards_for_current_episode)

        self.reset_environment()

    def update_cumulative_rewards(self, reward):
        """
        Updates the cumulative rewards for this episode.
        The cumulative rewards is added to the global rewards list and then resets after the episode is over.
        """
        self.cumulative_rewards_for_current_episode += reward
        self.iterations_for_current_episode += 1
        if self.environment.game_over():
            self.cumulative_rewards_per_episode.append(self.cumulative_rewards_for_current_episode)
            print("rewards for this episode: %s"%self.cumulative_rewards_for_current_episode)
            self.cumulative_rewards_for_current_episode = 0
            self.iterations_per_episode.append(self.iterations_for_current_episode)
            print("iterations for this episode: %s"%self.iterations_for_current_episode)
            self.iterations_for_current_episode = 0

    def train_input_fn(self):
        """
        The train input function for the tf.estimator.Estimator.
        """
        batch = self.data_buffer.get_batch(self.train_batch_size)
        s = 1.0 * np.array([d.s for d in batch])
        a = np.array([d.a for d in batch])
        r = 1.0 * np.array([d.r for d in batch])
        sp = 1.0 * np.array([d.sp for d in batch])
        # Masks whether we should count the max next score. 1 if not terminal, else 0.
        continue_mask = 1 - np.array([d.is_terminal for d in batch]).astype(np.int32)

        # onehot actions
        target_actions = np.stack(((a == Actions.NO_ACTION.value), (a == Actions.UP_ACTION.value))).T.astype(np.float64)

        # Calculate the max next score, which is 0 if we're at the end of an episode.
        max_next_score = np.amax(self.query_model(sp), axis=1) * continue_mask

        target_scores = r + self.gamma * max_next_score
        target_scores = target_scores.reshape((target_scores.shape[0], 1))

        features = {
            'images': tf.constant(s)
        }

        labels = {
            'target_scores': tf.constant(target_scores),
            'target_actions': tf.constant(target_actions),
            'latest_episode_score': tf.constant(self.cumulative_rewards_per_episode[-1]) if self.cumulative_rewards_per_episode else tf.constant(0),
            'latest_episode_iterations': tf.constant(self.iterations_per_episode[-1]) if self.iterations_per_episode else tf.constant(0)
        }

        return features, labels

    def train(self):
        """
        Trains the current in memory model with a batch of data.
        """
        if self.data_buffer.get_size() >= self.min_buffer_size_for_train:
            self.estimator.train(
                input_fn=self.train_input_fn,
                steps=self.train_steps_per_iteration,
            )

    def update_serving_model(self):
        """
        Saves the current in memory model to disk and use it as the new serving
        model.
        """
        features = {
            'images': tf.placeholder(dtype=tf.float64,
                                    shape=[1, self.image_width * self.image_height * len(Actions) / (self.image_shrink_factor * self.image_shrink_factor)])
        }
        self.estimator.export_savedmodel(
            export_dir_base=self.exported_model_dir,
            serving_input_receiver_fn=tf.estimator.export.build_raw_serving_input_receiver_fn(
                features=features
            )
        )

    def set_up_environment_for_iteration(self):
        """
        Sets up the environment, observations, and counters for this iteration.
        """
        self.iteration += 1
        if self.environment.game_over():
            self.previous_observation, self.current_observation, self.next_observation = None, None, None
            self.reset_environment()
        else:
            self.previous_observation, self.current_observation = self.current_observation, self.next_observation

    def get_action(self):
        """
        Gets an action based on the epsilon greedy algorithm. If we don't have both previous_observation and
        current_observation, we return NO_ACTION. Otherwise with probability epsilon we return a random action, and
        with probability 1-epsilon, we return the action with the highest predicted Q.
        """
        if (self.previous_observation is None) or (self.current_observation is None):
            return Actions.NO_ACTION.value
        if np.random.random() < self.epsilon:
            return np.random.choice(Actions).value
        else:
            state = Data.make_state(self.previous_observation, self.current_observation)
            scores = self.query_model_single(state)
            return Actions.get_action_from_index(np.argmax(scores)).value

    def query_model(self, states):
        """
        Queries the saved model for the scores of every action given a batch of 
        states.
        """
        request = predict_pb2.PredictRequest()
        request.model_spec.name = 'flappy'
        request.model_spec.signature_name = 'predicted_scores'
        request.inputs['images'].CopyFrom(
            tf.contrib.util.make_tensor_proto(states, shape=states.shape))
        try:
            response = self.stub.Predict(request, self.rpc_timeout)
            # print("good response")
        except grpc.framework.interfaces.face.face.AbortionError as error:
            print("abortion_error %s"%error)
            return 1.0 * np.zeros(shape=(states.shape[0], len(Actions)))

        return np.array(response.outputs['predicted_scores'].float_val).reshape(-1, len(Actions))

    def query_model_single(self, state):
        """
        Queries the saved mdoel for the scores of every action given a single
        state.
        """
        reshaped_state = state.reshape([1, state.shape[0]])
        return self.query_model(reshaped_state)[0]

    def model_fn(self, features, labels, mode, params):
        """
        Defines the model_fn for the trainer required by tf.estimator.Estimator.
        """
        height = self.image_height / self.image_shrink_factor
        width = self.image_width / self.image_shrink_factor
        inputs = tf.cast(tf.reshape(features['images'], [-1, height, width, len(Actions)]), tf.float32)
        tf.summary.image(
            name='screenshot',
            tensor=tf.reshape(tf.transpose(inputs, perm=(3, 0, 1, 2))[0], (self.train_batch_size,
                                                                           height,
                                                                           width,
                                                                           1))
        )

        conv = tf.contrib.layers.conv2d(
            inputs=inputs,
            num_outputs=32,
            kernel_size=3,
            stride=1,
            padding='SAME'
        )
        conv = tf.contrib.layers.max_pool2d(
            inputs=conv,
            kernel_size=2,
            stride=2,
            padding='VALID'
        )
        conv = tf.contrib.layers.conv2d(
            inputs=conv,
            num_outputs=64,
            kernel_size=3,
            stride=1,
            padding='SAME'
        )
        conv = tf.contrib.layers.max_pool2d(
            inputs=conv,
            kernel_size=2,
            stride=2,
            padding='VALID'
        )
        conv = tf.contrib.layers.conv2d(
            inputs=conv,
            num_outputs=128,
            kernel_size=3,
            stride=1,
            padding='SAME'
        )

        # This is set to the shrink factor from max_pool2d
        pool_shrink_factor = 4
        fc = tf.contrib.layers.fully_connected(
            inputs=tf.reshape(conv, [-1, (height / pool_shrink_factor) * (width / pool_shrink_factor) * 128]),
            num_outputs=256,
        )

        fc = tf.contrib.layers.dropout(
            inputs=fc,
            keep_prob=0.5,
        )

        predictions = tf.contrib.layers.fully_connected(
            inputs=fc,
            num_outputs=len(Actions),
            activation_fn=None,
        )

        export_outputs = {
            'predicted_scores': tf.estimator.export.PredictOutput({
                'predicted_scores': predictions
            })
        }

        if mode == tf.estimator.ModeKeys.PREDICT:
            return tf.estimator.EstimatorSpec(
                mode=mode,
                predictions=predictions,
                export_outputs=export_outputs
            )

        # Last episode's score
        tf.summary.scalar(
            name='latest_episode_score',
            tensor=labels['latest_episode_score']
        )

        # Last episode's number of iterations
        tf.summary.scalar(
            name='latest_episode_iterations',
            tensor=labels['latest_episode_iterations']
        )

        # [-1, 1]
        target_scores = tf.cast(labels['target_scores'], tf.float32)

        tf.summary.scalar(
            name='average_target_q',
            tensor=tf.reduce_mean(target_scores)
        )

        # [-1, 2] one-hot
        target_actions = tf.cast(labels['target_actions'], tf.float32)

        # [-1, 1]
        predicted_scores = tf.reshape(tf.reduce_sum(
            tf.multiply(target_actions, predictions), axis=1), (-1, 1))

        tf.summary.scalar(
            name='average_predicted_q',
            tensor=tf.reduce_mean(predicted_scores)
        )

        loss = tf.losses.mean_squared_error(
            labels=target_scores, predictions=predicted_scores)

        train_op = tf.train.AdamOptimizer(params['learning_rate']).minimize(
            loss=loss, global_step=tf.train.get_global_step())

        eval_metric_ops = {}

        return tf.estimator.EstimatorSpec(
            mode=mode,
            loss=loss,
            train_op=train_op,
            eval_metric_ops=eval_metric_ops
        )
